﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartConductor.Models
{
    public class Request
    {
        [Required(ErrorMessage = "Invalid station!")]
        public string Start { get; set; }
        [Required(ErrorMessage = "Invalid station!")]
        public string End { get; set; }

        [DataType(DataType.DateTime, ErrorMessage = "Invalid station!")]
        public DateTime Date
        {
            get
            {
                try
                {
                    return Convert.ToDateTime(DateString);
                }
                catch (Exception)
                {
                    return new DateTime();
                }

            }
            set
            {
                this.Date = value;
            }
        }

        [Range(typeof(DateTime), "1/1/1880", "1/1/2200", ErrorMessage = "Invalid date!")]
        public string DateString { get; set; }

        public Request(string start, string end, DateTime date)
        {
            this.Start = start;
            this.End = end;
            this.Date = date;
        }

        public Request()
        {

        }

        public override bool Equals(Object obj)
        {
            Request requestObj = obj as Request;
            if (requestObj == null)
                return false;
            else
                return Start.Equals(requestObj.Start) && End.Equals(requestObj.End) && Date.Equals(requestObj.Date);
        }

        public override int GetHashCode()
        {
            if (Start!=null && End!=null)
            {
                return (this.Start.GetHashCode() + this.End.GetHashCode()) % int.MaxValue;    
            }
            return base.GetHashCode();
        }
    }
}