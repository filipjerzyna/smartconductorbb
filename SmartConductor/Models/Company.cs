﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartConductor.Models
{
    [Serializable]
    public class Company
    {
        public int CompanyId { get; set; }
        public string Name { get; set; }
    }
}
