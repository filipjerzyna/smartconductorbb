﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Windows;

namespace SmartConductor.Models
{
    public class MyRectangle
    {
        Point R1 { get; set; }
        Point R2 { get; set; }
        Point R3 { get; set; }
        Point R4 { get; set; }

        public MyRectangle(Point point1, Point point2, double expectedWidth)
        {
            Vector v = new Vector();
            Vector p = new Vector();
            Vector n = new Vector();
            Point r1 = new Point();
            Point r2 = new Point();
            Point r3 = new Point();
            Point r4 = new Point();

            v.X = point2.X - point1.X;
            v.Y = point2.Y - point1.Y;
            //Then calculate a perpendicular to it (just swap X and Y coordinates)

            p.X = v.Y; //Use separate variable otherwise you overwrite X coordinate here
            p.Y = -v.X; //Flip the sign of either the X or Y (edit by adam.wulf)
            //Normalize that perpendicular

            double length = Math.Sqrt(p.X * p.X + p.Y * p.Y); //Thats length of perpendicular
            n.X = p.X / length;
            n.Y = p.Y / length; //Now n is normalized perpendicular
            //Calculate 4 points that form a rectangle by adding normalized perpendicular and multiplying it by half of the desired width

            r1.X = point1.X + n.X * expectedWidth / 2;
            r1.Y = point1.Y + n.Y * expectedWidth / 2;
            r2.X = point1.X - n.X * expectedWidth / 2;
            r2.Y = point1.Y - n.Y * expectedWidth / 2;
            r3.X = point2.X - n.X * expectedWidth / 2;
            r3.Y = point2.Y - n.Y * expectedWidth / 2;
            r4.X = point2.X + n.X * expectedWidth / 2;
            r4.Y = point2.Y + n.Y * expectedWidth / 2;



            R1 = r1;
            R2 = r2;
            R3 = r3;
            R4 = r4;

        }

        public bool Contains(Point point)
        {
            //edge lengths
            double a1 = Math.Round(Math.Sqrt(Math.Pow(R1.X - R2.X, 2) + Math.Pow(R1.Y - R2.Y, 2)),5);
            double a2 = Math.Round(Math.Sqrt(Math.Pow(R2.X - R3.X, 2) + Math.Pow(R2.Y - R3.Y, 2)),5);
            double a3 = Math.Round(Math.Sqrt(Math.Pow(R3.X - R4.X, 2) + Math.Pow(R3.Y - R4.Y, 2)),5);
            double a4 = Math.Round(Math.Sqrt(Math.Pow(R4.X - R1.X, 2) + Math.Pow(R4.Y - R1.Y, 2)),5);

            //calculate the lengths of the line segments
            double b1 = Math.Round(Math.Sqrt(Math.Pow(R1.X - point.X, 2) + Math.Pow(R1.Y - point.Y, 2)),5);
            double b2 = Math.Round(Math.Sqrt(Math.Pow(R2.X - point.X, 2) + Math.Pow(R2.Y - point.Y, 2)),5);
            double b3 = Math.Round(Math.Sqrt(Math.Pow(R3.X - point.X, 2) + Math.Pow(R3.Y - point.Y, 2)),5);
            double b4 = Math.Round(Math.Sqrt(Math.Pow(R4.X - point.X, 2) + Math.Pow(R4.Y - point.Y, 2)),5);


            //check corectness of calculated rectangle
            if (!(a1 * a2 == a2 * a3 && a2 * a3 == a3 * a4 && a3 * a4 == a4 * a1))
            {
                Console.Out.WriteLine("Oops we've got a problem here");
            }
            //calculating areas
            double A = Math.Round(a1 * a2,0);
            double u1 = Math.Round(((a1 + b1 + b2) / 2),5);
            double u2 = Math.Round(((a2 + b2 + b3) / 2),5);
            double u3 = Math.Round(((a3 + b3 + b4) / 2),5);
            double u4 = Math.Round(((a4 + b4 + b1) / 2),5);

            double A1 = Math.Round(Math.Sqrt(u1 * (u1 - a1) * (u1 - b1) * (u1 - b2)), 5);
            double A2 = Math.Round(Math.Sqrt(u2 * (u2 - a2) * (u2 - b2) * (u2 - b3)), 5);
            double A3 = Math.Round(Math.Sqrt(u3 * (u3 - a3) * (u3 - b3) * (u3 - b4)), 5);
            double A4 = Math.Round(Math.Sqrt(u4 * (u4 - a4) * (u4 - b4) * (u4 - b1)), 5);
            bool pointIsInsideTheRectangle = A == Math.Round((A1 + A2 + A3 + A4), 0);
            return pointIsInsideTheRectangle;
        }
    }
}