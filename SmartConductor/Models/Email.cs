﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartConductor.Models
{
    public class Email
    {
        [StringLength(100)]
        [Required]
        public string Name { get; set; }
        [StringLength(100)]
        [Required]
        public string Surname { get; set; }
        [EmailAddress(ErrorMessage="The Email address is invalid.")]
        [Required]
        [Display(Name="Email")]
        public string EmailAddress { get; set; }
        [StringLength(1000)]
        [Required]
        public string Message { get; set; }

    }
}