﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using SmartConductor.Models;
using System.Windows;

namespace SmartConductor.Models
{
    public class RailwayMap
    {
        internal IDictionary<string, RailwayStation> Stations { get; set; }

        public RailwayMap()
        {
            Stations = new Dictionary<string, RailwayStation>();
        }

        public void AddStation(string name)
        {
            var node = new RailwayStation(name);
            Stations.Add(name, node);
        }

        public void AddStation(string name, Point location)
        {
            var node = new RailwayStation(name);
            node.Latitude = location.X;
            node.Longitude = location.Y;
            Stations.Add(name, node);
        }

        public void RemoveConnection(Connection connection)
        {
            Stations[connection.Start.Name].RemoveConnection(connection);
        }

        public void AddConnection(Connection connection)
        {
            Stations[connection.Start.Name].AddConnection(connection);
        }

        public void AddConnection(string from, string to, int distance, DateTime departureTime)
        {
            Stations[from].AddConnection(Stations[to], distance, departureTime);
        }

        public void AddReversedConnection(string from, string to, int distance, DateTime departureTime, TrainType type)
        {
            Stations[to].AddReversedConnection(Stations[from], distance, departureTime, type);
        }

        public RailwayMap GetReversedMap()
        {
            RailwayMap reversedMap = new RailwayMap();
            Stations.Keys.ToList().ForEach(o => reversedMap.AddStation(o, Stations[o].Location));
            foreach (var station in Stations.Values)
            {
                station.Connections.ToList().ForEach(o => reversedMap.AddReversedConnection(o.Start.Name, o.End.Name, o.TravelTime, o.DepartureTime, o.TrainType));
            }


            return reversedMap;
        }
    }
}
