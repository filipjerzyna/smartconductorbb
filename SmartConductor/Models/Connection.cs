﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SmartConductor.Models;
using System.Web;
using SmartConductor.Infrastructure;

namespace SmartConductor.Models
{
    [Serializable]
    public class Connection : IEquatable<Connection>
    {
        public int ConnectionId { get; set; }
        public bool IsYearMonthDayIrrelevant = false;
        public int StartId { get; set; }
        public int TrainTypeId { get; set; }

        [ForeignKey("StartId")]
        public virtual RailwayStation Start { get; set; }

        public int EndId { get; set; }

        [ForeignKey("EndId")]
        public virtual RailwayStation End { get; set; }

        public int TravelTime { get; set; }

        private DateTime departureTime;

        public DateTime DepartureTime
        {
            get { return this.departureTime; }

            set
            {
                this.departureTime = value;
            }
        }

        private DateTime arrivalTime;

        public DateTime ArrivalTime
        {
            get { return this.arrivalTime; }
            set { this.arrivalTime = value; }
        }


        public Connection()
        { }

        public virtual TrainType TrainType { get; set; }

        public double Price { get; set; }

        public Connection(RailwayStation target, int distance)
        {
            End = target;
            this.TravelTime = distance;
        }


        public Connection(RailwayStation source, RailwayStation target, int distance, DateTime departureTime)
        {
            Start = source;
            End = target;
            this.TravelTime = distance;
            this.DepartureTime = departureTime;
            this.ArrivalTime = departureTime.Add(new TimeSpan(0, distance, 0));
        }

        public Connection(RailwayStation source, RailwayStation target, int distance, int hour, int minute, TrainType trainType)
            : this(source, target, distance, hour, minute)
        {
            TrainTypeId = trainType.TrainTypeId;
            TrainType = trainType;
        }

        public Connection(RailwayStation source, RailwayStation target, int distance, int hour, int minute)
        {
            Start = source;
            End = target;
            this.TravelTime = distance;
            //Minimal value in Microsoft SQL = 1753-01-01 00:00:00  
            this.DepartureTime = new DateTime(1753, 01, 01, hour, minute, 0);
            this.ArrivalTime = DepartureTime.Add(new TimeSpan(0, distance, 0));
        }

        public override string ToString()
        {
            return "Start: " + Start.Name + " target:" + End.Name + " TravelTime:" + TravelTime + " departureTime:" + DepartureTime.Hour + ":" + DepartureTime.Minute + " Arrival:" + ArrivalTime.Hour + ":" + ArrivalTime.Minute;
        }

        public string ToString(DateTime actualDate)
        {
            return "Start: " + Start.Name + " target:" + End.Name + " TravelTime:" + TravelTime + " departureTime:" + DepartureTime.Hour + ":" + DepartureTime.Minute + " Arrival:" + ArrivalTime.Hour + ":" + ArrivalTime.Minute + " Departure Date:" + (IsYearMonthDayIrrelevant ? actualDate.ToShortDateString() : DepartureTime.ToShortDateString());
        }

        public string ToHtmlTableRow(DateTime actualDate)
        {
            return "<td>" + Start.Name + "<td/><td>" + End.Name + "</td><td>" + TravelTime + " </td><td>" + DepartureTime.Hour + ":" + DepartureTime.Minute + "<td/><td>" + ArrivalTime.Hour + ":" + ArrivalTime.Minute + "<td/><td>" + (IsYearMonthDayIrrelevant ? actualDate.ToShortDateString() : DepartureTime.ToShortDateString()) + "<td/>";
        }


        public string GetHourMinuteString(DateTime date)
        {
            return date.Hour.ToString("00") + ":" + date.Minute.ToString("00");
        }

        public string GetShortDateString(DateTime actualDate)
        {
            return (IsYearMonthDayIrrelevant ? actualDate.ToShortDateString() : ArrivalTime.ToShortDateString());
        }
        public void ReverseProperties()
        {
            RailwayStation tempStation;
            int tempInt;
            DateTime tempDateTime;

            tempStation = End;
            End = Start;
            Start = tempStation;

            tempInt = EndId;
            EndId = StartId;
            StartId = tempInt;

            tempDateTime = DepartureTime;
            DepartureTime = ArrivalTime;
            ArrivalTime = tempDateTime;
        }

        public Connection GetReversedDeepCopy()
        {
            var deepCopy = this.DeepClone();
            deepCopy.ReverseProperties();
            return deepCopy;
        }

        public Connection GetShallowCopy()
        {
            var shallowCopy = new Connection(this.Start, this.End, this.TravelTime, this.DepartureTime.Hour, this.DepartureTime.Minute, this.TrainType);
            return shallowCopy;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Connection p = obj as Connection;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            bool equality=(p.Start.Name.Equals(this.Start.Name)) && (p.End.Name.Equals(this.End.Name)) && (p.TravelTime.Equals(this.TravelTime)) && (p.DepartureTime.Equals(this.DepartureTime));
            return equality;
        }



        public bool Equals(Connection p)
        {
            // If parameter is null return false:
            if ((object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            bool equality = (p.Start.Name.Equals(this.Start.Name)) && (p.End.Name.Equals(this.End.Name)) && (p.TravelTime.Equals(this.TravelTime)) && (p.DepartureTime.Equals(this.DepartureTime));
            return equality;
        }

        public override int GetHashCode()
        {
            return ArrivalTime.Millisecond + TravelTime;
        }
    }
}
