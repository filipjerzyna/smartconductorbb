﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SmartConductor.Models;
using System.Windows;
using System.Collections;

namespace SmartConductor.Models
{
    [Serializable]
    public class RailwayStation : IEquatable<RailwayStation>
    {
        public int RailwayStationId { get; set; }

        [InverseProperty("Start")]
        public virtual ICollection<Connection> Connections { get; set; }

        [StringLength(150)]
        public string Name { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        [NotMapped]
        public Point Location {
            get { return new Point(Latitude, Longitude); }
            set { value.X = Latitude; value.Y = Longitude; } }

        [NotMapped]
        internal double DistanceFromTheStart { get; set; }

        [NotMapped]
        public RailwayStation PreviousStation { get; set; }

        [NotMapped]
        public Connection ConnectionToPreviousStation { get; set; }

        [NotMapped]
        int HashCode { get { return base.GetHashCode(); } }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        internal RailwayStation(string name)
        {
            this.Name = name;
            Connections = new List<Connection>();
        }

        internal RailwayStation(string name, double latitude, double longitude)
        {
            this.Name = name;
            this.Latitude=latitude;
            this.Longitude = longitude;
            Connections = new List<Connection>();
        }

        public RailwayStation()
        {
        }


        internal void AddConnection(RailwayStation targetNode, int distance, DateTime departureTime)
        {
            Connections.Add(new Connection(this, targetNode, distance, departureTime));
        }

        internal void RemoveConnection(Connection connection)
        {
            Connections.Remove(connection);
        }

        internal void AddReversedConnection(RailwayStation targetNode, int distance, DateTime departureTime, TrainType type)
        {
            var connection = new Connection();
            connection.End = targetNode;
            connection.Start = this;
            connection.ArrivalTime = departureTime;
            connection.DepartureTime = departureTime.AddMinutes(distance);
            connection.TravelTime = distance;
            connection.TrainType = type;
            Connections.Add(connection);
        }

        internal void AddConnection(Connection connection)
        {
            Connections.Add(connection);
        }

        public bool Equals(RailwayStation other)
        {
            // If parameter is null return false:
            if ((object)other == null)
            {
                return false;
            }

            // Return true if the fields match:
            bool conditions = Name.Equals(other.Name);
            return conditions;
        }

        public static int CompareByDistanceFromTheStart(RailwayStation station1, RailwayStation station2)
        {
            return station1.DistanceFromTheStart.CompareTo(station2.DistanceFromTheStart);
        }

        public static void UpdateDistance(RailwayStation station, double newDistance)
        {
            station.DistanceFromTheStart = newDistance;
        }  

    }

    public class RailwayStationComparer : IComparer<RailwayStation>
    {

        int IComparer<RailwayStation>.Compare(RailwayStation x, RailwayStation y)
        {
            return x.DistanceFromTheStart.CompareTo(y.DistanceFromTheStart);
        }
    }
}
