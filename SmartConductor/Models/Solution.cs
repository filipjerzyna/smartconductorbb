﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SmartConductor.Infrastructure;

namespace SmartConductor.Models
{
    public class Solution : IEquatable<Solution>
    {
        private DateTime desiredDateTime;
        private List<Connection> path;
        private bool departureOriented;
        long cost;

        public bool IsEmpty
        {
            get { return path.Count > 0 ? false : true; }

        }

        public bool DepartureOriented
        {
            get { return departureOriented; }
            set { departureOriented = value; }
        }

        public DateTime DesiredDateTime
        {
            get { return desiredDateTime; }
            set { desiredDateTime = value; }
        }

        public List<Connection> Path
        {
            get { return path; }
            set { path = value; }
        }

        public long Cost
        {
            get { return cost; }
            set { cost = value; }
        }

        public Solution()
        {
            cost = -1;
            departureOriented = true;
        }

        public Solution(List<Connection> path, DateTime desiredTime, bool departureOriented)
        {
            this.DepartureOriented = departureOriented;
            this.Path = path;
            this.DesiredDateTime = desiredTime;
            long tempCost = 0;

            if (path.Count > 0)
            {
                long transferTime = 0;
                for (int i = 0; i < this.path.Count; i++)
                {
                    tempCost += this.path[i].TravelTime;
                    if (i != 0)
                    {
                        transferTime = Convert.ToInt64(Utilities.CalculateTimeSpanWithoutDate(this.path[i - 1].ArrivalTime, this.path[i].DepartureTime));
                        tempCost += transferTime;
                    }
                }

                if (departureOriented)
                {
                    tempCost += Convert.ToInt64(Utilities.CalculateTimeSpanWithoutDate(path[0].DepartureTime, desiredDateTime));
                }
                else
                {
                    tempCost += Convert.ToInt64(Utilities.CalculateTimeSpanWithoutDate(path[path.Count - 1].ArrivalTime, desiredDateTime));
                }
                this.Cost = tempCost;
            }
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Solution p = obj as Solution;
            if ((System.Object)p == null)
            {
                return false;
            }

            bool pathsEquality = true;
            if (p.Path.Count == this.Path.Count)
            {
                for (int i = 0; i < p.Path.Count; i++)
                {
                    if (!p.Path[i].Equals(this.Path[i]))
                    {
                        pathsEquality = false;
                        break;
                    }
                }
            }
            else
            {
                pathsEquality = false;
            }

            //return (pathsEquality && p.DesiredDateTime.Equals(DesiredDateTime) && p.Cost.Equals(Cost) && p.DepartureOriented.Equals(DepartureOriented));
            return (pathsEquality && p.Cost.Equals(this.Cost));
        }



        public bool Equals(Solution p)
        {
            // If parameter is null return false:
            if ((object)p == null)
            {
                return false;
            }

            bool pathsEquality = true;
            if (p.Path.Count == this.Path.Count)
            {
                for (int i = 0; i < p.Path.Count; i++)
                {
                    if (!p.Path[i].Equals(this.Path[i]))
                    {
                        pathsEquality = false;
                        break;
                    }
                }
            }
            else
            {
                pathsEquality = false;
            }

            // Return true if the fields match:
            //return (pathsEquality && p.DesiredDateTime.Equals(DesiredDateTime) && p.Cost.Equals(Cost) && p.DepartureOriented.Equals(DepartureOriented));

            return (pathsEquality && p.Cost.Equals(this.Cost));
        }

        public override int GetHashCode()
        {
            return Convert.ToInt32(cost * Path.Count);
        }

    }
}