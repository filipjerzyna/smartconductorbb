﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartConductor.Models;

namespace SmartConductor.Models
{
    [Serializable]
    public class TrainType
    {
        public int TrainTypeId { get; set; }
        public int CompanyId { get; set; }
        public string ImagePath { get; set; }
        public string Name { get; set; }
        public virtual Company Company { get; set; }
    }
}
