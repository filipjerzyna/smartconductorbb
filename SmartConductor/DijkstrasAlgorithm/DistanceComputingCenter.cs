﻿using SmartConductor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartConductor.Infrastructure;
using System.Windows;
using DIBRIS.Hippie;

namespace SmartConductor.DijkstraAlgorithm
{
    public class DistanceComputingCenter
    {
        private static double SearchingWidth = 15;
        public List<Solution> ComputeSolutions(RailwayMap map, string startingStation, string endingStation, DateTime desiredDeparture)
        {
            List<Solution> availableSolutions = new List<Solution>();
            if (!map.Stations.Keys.Contains(startingStation) || !map.Stations.Keys.Contains(endingStation))
            {
                return availableSolutions;
            }

            MyRectangle searchingArea = new MyRectangle(map.Stations[startingStation].Location, map.Stations[endingStation].Location, SearchingWidth);


            ExecuteDijkstrasAlgortihm(map, startingStation, desiredDeparture, searchingArea);
            List<Connection> bestPath = ExtractAListOfConnections(map, endingStation);
            int total = bestPath.Count;


            if (total > 0)
            {

                availableSolutions.Add(new Solution(bestPath, desiredDeparture, true));

                Connection connectionToDisable = null;
                Connection secondConnectionToDisable = null;
                Solution newSolution;
                for (int i = 0; i < total; i++)
                {
                    connectionToDisable = bestPath[i];
                    map.RemoveConnection(connectionToDisable);
                    ExecuteDijkstrasAlgortihm(map, startingStation, desiredDeparture, searchingArea);
                    var newListOfConnections = ExtractAListOfConnections(map, endingStation);
                    if (newListOfConnections.Count > 0)
                    {
                        newSolution = new Solution(newListOfConnections, desiredDeparture, true);
                        availableSolutions.Add(newSolution);
                    }
                    map.AddConnection(connectionToDisable);

                }


                //in case of the single path solution, we try to obtain a one more
                if (bestPath.Count == 1 && availableSolutions.Count == 2 && connectionToDisable != null)
                {
                    secondConnectionToDisable = connectionToDisable;
                    connectionToDisable = availableSolutions[1].Path[0];
                    map.RemoveConnection(connectionToDisable);
                    map.RemoveConnection(secondConnectionToDisable);
                    ExecuteDijkstrasAlgortihm(map, startingStation, desiredDeparture, searchingArea);
                    newSolution = new Solution(ExtractAListOfConnections(map, endingStation), desiredDeparture, true);
                    if (!newSolution.IsEmpty)
                    {
                        availableSolutions.Add(newSolution);
                    }

                    map.AddConnection(connectionToDisable);
                    map.AddConnection(secondConnectionToDisable);
                }

                availableSolutions = availableSolutions.Distinct().Where(g => g.Path.Count > 0).OrderBy(o => o.Cost).Take(3).ToList();


            }
            return availableSolutions;
        }



        internal List<Solution> ComputeReversedSolutions(RailwayMap map, string startingStation, string endingStation, DateTime desiredArrival)
        {
            List<Solution> availableSolutions = new List<Solution>();
            if (!map.Stations.Keys.Contains(startingStation) || !map.Stations.Keys.Contains(endingStation))
            {
                return availableSolutions;
            }
            MyRectangle searchingArea = new MyRectangle(map.Stations[endingStation].Location, map.Stations[startingStation].Location, SearchingWidth);
            ExecuteReversedDijkstrasAlgortihm(map, startingStation, desiredArrival, searchingArea);
            // element at 0 position is the last connection
            List<Connection> reversedBestPath = ExtractAListOfConnections(map, endingStation);
            List<Connection> bestPath = ExtractReversedSolution(map, endingStation);

            int total = bestPath.Count;
            if (total != 0)
            {
                availableSolutions.Add(new Solution(bestPath, desiredArrival, false));

                Connection connectionToDisable = null;
                Connection secondConnectionToDisable = null;
                Solution newSolution;
                for (int i = 0; i < total; i++)
                {
                    connectionToDisable = reversedBestPath[i];
                    map.RemoveConnection(connectionToDisable);
                    ExecuteReversedDijkstrasAlgortihm(map, startingStation, desiredArrival, searchingArea);
                    List<Connection> reversedNextPath = ExtractAListOfConnections(map, endingStation);
                    newSolution = new Solution(ExtractReversedSolution(map, endingStation), desiredArrival, false);
                    if (!newSolution.IsEmpty)
                    {
                        availableSolutions.Add(newSolution);


                        if (total == 1)
                        {
                            secondConnectionToDisable = reversedNextPath[0];
                            map.RemoveConnection(secondConnectionToDisable);
                            ExecuteReversedDijkstrasAlgortihm(map, startingStation, desiredArrival, searchingArea);
                            newSolution = new Solution(ExtractReversedSolution(map, endingStation), desiredArrival, false);
                            if (!newSolution.IsEmpty)
                            {
                                availableSolutions.Add(newSolution);
                            }

                            map.AddConnection(secondConnectionToDisable);
                        }

                    }
                    map.AddConnection(connectionToDisable);
                }

                availableSolutions = availableSolutions.Distinct().Where(g => g.Path.Count > 0).OrderBy(o => o.Cost).Take(3).ToList();
            }
            return availableSolutions;
        }

        private void ExecuteDijkstrasAlgortihm(RailwayMap map, string startingStation, DateTime desiredDeparture, MyRectangle area)
        {
            InitialiseMap(map, startingStation);
            ProcessMap(map, startingStation, desiredDeparture, area);
        }

        private void ExecuteReversedDijkstrasAlgortihm(RailwayMap map, string startingStation, DateTime desiredArrival, MyRectangle area)
        {
            InitialiseMap(map, startingStation);
            ProcessReversedMap(map, startingStation, desiredArrival, area);
        }

        private void InitialiseMap(RailwayMap map, string startingStation)
        {
            foreach (RailwayStation station in map.Stations.Values)
            {
                station.DistanceFromTheStart = double.PositiveInfinity;
                station.ConnectionToPreviousStation = null;
                station.PreviousStation = null;
            }
            map.Stations[startingStation].DistanceFromTheStart = 0;
        }

        //Version 2 with taking into account desired departureTime time
        private void ProcessMap(RailwayMap map, string startingStation, DateTime desiredDeparture, MyRectangle area)
        {
            bool finished = false;
            var notVisitedStations = map.Stations.Values.ToList();
            Dictionary<RailwayStation, IHeapHandle<RailwayStation, double>> handles = new Dictionary<RailwayStation, IHeapHandle<RailwayStation, double>>();
            FibonacciHeap<RailwayStation, double> heap = HeapFactory.NewRawFibonacciHeap<RailwayStation, double>();
            RailwayStation nextStation;
            notVisitedStations.ForEach(g => handles.Add(g, heap.Add(g, g.DistanceFromTheStart)));

            while (!finished && notVisitedStations.Count > 0)
            {
                nextStation = heap.Min.Value;


                if (nextStation != null)
                {
                    if (area.Contains(nextStation.Location))
                    {
                        ProcessStationWithDesiredArrivalTime(nextStation, handles, notVisitedStations, heap, desiredDeparture, area);
                    }
                    heap.Remove(handles[nextStation]);
                    notVisitedStations.Remove(nextStation);


                }
                else
                {
                    finished = true;
                }
            }
        }

        private void ProcessReversedMap(RailwayMap map, string startingStation, DateTime desiredArrival, MyRectangle area)
        {
            bool finished = false;
            var notVisitedStations = map.Stations.Values.ToList();

            Dictionary<RailwayStation, IHeapHandle<RailwayStation, double>> handles = new Dictionary<RailwayStation, IHeapHandle<RailwayStation, double>>();
            FibonacciHeap<RailwayStation, double> heap = HeapFactory.NewRawFibonacciHeap<RailwayStation, double>();
            RailwayStation nextStation;
            notVisitedStations.ForEach(g => handles.Add(g, heap.Add(g, g.DistanceFromTheStart)));


            while (!finished && notVisitedStations.Count > 0)
            {
                nextStation = heap.Min.Value;
                if (nextStation != null)
                {
                    if (area.Contains(nextStation.Location))
                    {
                        ProcessReversedStationWithDesiredArrivalTime(nextStation, handles, notVisitedStations, heap, desiredArrival, area);
                    }
                    heap.Remove(handles[nextStation]);
                    notVisitedStations.Remove(nextStation);
                }
                else
                {
                    finished = true;
                }
            }
        }

        private void ProcessStationWithDesiredArrivalTime(RailwayStation station, Dictionary<RailwayStation, IHeapHandle<RailwayStation, double>> handles, List<RailwayStation> notVisitedStations, FibonacciHeap<RailwayStation, double> heap, DateTime desiredTime, MyRectangle area, int? transferTime = 1)
        {
            var connections = station.Connections.Where(c => notVisitedStations.Contains(c.End)).Where(d => area.Contains(d.Start.Location)).Where(d => area.Contains(d.End.Location));

            connections = ProcessConnections(connections, desiredTime);
            foreach (var connection in connections)
            {
                double distance;
                if (station.ConnectionToPreviousStation != null && station.ConnectionToPreviousStation.ArrivalTime != null)
                {
                    distance = station.DistanceFromTheStart + connection.TravelTime + Utilities.CalculateTimeSpan(station.ConnectionToPreviousStation.ArrivalTime, connection.DepartureTime);
                }
                else
                {
                    distance = station.DistanceFromTheStart + connection.TravelTime + Utilities.CalculateTimeSpan(desiredTime, connection.DepartureTime);
                }


                if ((distance < connection.End.DistanceFromTheStart) && (station.ConnectionToPreviousStation == null || (CompareDates(connection.DepartureTime, station.ConnectionToPreviousStation.ArrivalTime.AddMinutes((double)transferTime)))))
                {

                    heap.UpdatePriorityOf(handles[connection.End], distance);

                    connection.End.DistanceFromTheStart = distance;
                    connection.End.PreviousStation = station;
                    connection.End.ConnectionToPreviousStation = connection;


                }

            }
        }


        private IEnumerable<Connection> ProcessConnections(IEnumerable<Connection> connections, DateTime desiredTime, bool departureOriented = true)
        {
            var tempConnections = new List<Connection>();
            foreach (var connection in connections)
            {
                //Quick hack - needs to implement an additional field in the Connection class.
                //This field should mark a connection whether it is an everyday rule. 
                if (connection.DepartureTime.Year == 1753)
                {

                    DateTime arrivalTime = connection.ArrivalTime;
                    connection.ArrivalTime = new DateTime(desiredTime.Year, desiredTime.Month, desiredTime.Day, arrivalTime.Hour, arrivalTime.Minute, 0);

                    DateTime departureTime = connection.DepartureTime;
                    connection.DepartureTime = new DateTime(desiredTime.Year, desiredTime.Month, desiredTime.Day, departureTime.Hour, departureTime.Minute, 0);
                    /*
                    Connection tempConnection = connection.GetShallowCopy();
                    
                     // Searching with taking into account the next day or the day before
                    if (departureOriented)
                    {
                        tempConnection.ArrivalTime = connection.ArrivalTime.AddDays(1);
                        tempConnection.DepartureTime = connection.DepartureTime.AddDays(1);
                    }
                    else
                    {
                        tempConnection.ArrivalTime = connection.ArrivalTime.AddDays(-1);
                        tempConnection.DepartureTime = connection.DepartureTime.AddDays(-1);
                    }
                    
                    tempConnections.Add(tempConnection);
                       */
                }
            }

            tempConnections.AddRange(connections);
            return tempConnections;
        }


        private void ProcessReversedStationWithDesiredArrivalTime(RailwayStation station, Dictionary<RailwayStation, IHeapHandle<RailwayStation, double>> handles, List<RailwayStation> notVisitedStations, FibonacciHeap<RailwayStation, double> heap, DateTime desiredArrival, MyRectangle area, int? transferTime = 1)
        {
            var connections = station.Connections.Where(c => notVisitedStations.Contains(c.End)).Where(d => area.Contains(d.Start.Location)).Where(d => area.Contains(d.End.Location));
            connections = ProcessConnections(connections, desiredArrival, false);
            foreach (var connection in connections)
            {
                double distance;

                //Counting distance for the rest of the path.
                if (station.ConnectionToPreviousStation != null && station.ConnectionToPreviousStation.ArrivalTime != null)
                {
                    distance = station.DistanceFromTheStart + connection.TravelTime + Utilities.CalculateTimeSpan(station.ConnectionToPreviousStation.ArrivalTime, connection.DepartureTime);
                }
                else
                //Counting distance for the first station in the road.
                {
                    distance = station.DistanceFromTheStart + connection.TravelTime + Utilities.CalculateTimeSpan(desiredArrival, connection.DepartureTime);
                }


                if ((distance < connection.End.DistanceFromTheStart) && (station.ConnectionToPreviousStation == null || (CompareDates(station.ConnectionToPreviousStation.ArrivalTime.AddMinutes(-(double)transferTime), connection.DepartureTime))))
                {
                    heap.UpdatePriorityOf(handles[connection.End], distance);

                    connection.End.DistanceFromTheStart = distance;
                    connection.End.PreviousStation = station;
                    connection.End.ConnectionToPreviousStation = connection;
                }

            }
        }


        /// <summary>
        /// Allows to compare DateTimes by taking into account only hours and minutes.
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns>true if date1 is bigger, false otherwise</returns>
        private bool CompareHoursAndMinutes(DateTime date1, DateTime date2)
        {
            //Making a copy in order to compare only hour and minutes between two dates
            DateTime date1Temp = new DateTime(date2.Year, date2.Month, date2.Day, date1.Hour, date1.Minute, date1.Millisecond);
            double difference = (date2 - date1Temp).TotalMinutes;
            if (difference > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Allows to compare DateTimes.
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns>true if date1 is bigger, false otherwise</returns>
        private bool CompareDates(DateTime date1, DateTime date2)
        {
            bool comparisonResult = date1.CompareTo(date2) >= 0 ? true : false;
            return comparisonResult;
            //double difference = (date2 - date1).TotalMinutes;
            //if (difference > 0)
            //{
            //    return false;
            //}
            //else
            //{
            //    return true;
            //}
        }

        private IDictionary<string, Connection> ExtractPaths(RailwayMap graph)
        {
            return graph.Stations.ToDictionary(n => n.Key, n => n.Value.ConnectionToPreviousStation);
        }

        private List<Connection> ExtractAListOfConnections(RailwayMap graph, string endStation)
        {
            var paths = ExtractPaths(graph);
            var list = new List<Connection>();
            bool noPreviousStation = false;

            Connection tempConnection = paths[endStation];

            while (!noPreviousStation)
            {
                if (tempConnection == null)
                {
                    noPreviousStation = true;
                }
                else
                {
                    list.Add(tempConnection);
                    tempConnection = tempConnection.Start.ConnectionToPreviousStation;
                }
            }

            list.Reverse();

            return list;
        }
        private List<Connection> ExtractReversedSolution(RailwayMap graph, string endStation)
        {
            var paths = ExtractPaths(graph);
            var list = new List<Connection>();
            bool noPreviousStation = false;

            Connection tempConnection = paths[endStation];

            while (!noPreviousStation)
            {
                if (tempConnection == null)
                {
                    noPreviousStation = true;
                }
                else
                {
                    list.Add(tempConnection.GetReversedDeepCopy());
                    tempConnection = tempConnection.Start.ConnectionToPreviousStation;
                }
            }


            return list;
        }

    }
}
