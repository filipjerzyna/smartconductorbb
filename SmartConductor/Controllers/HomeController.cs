﻿using SmartConductor;
using SmartConductor.DAL;
using SmartConductor.Models;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmartConductor.Infrastructure;
using SmartConductor.DijkstraAlgorithm;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.Drawing.Imaging;
using System.Reflection;
using System.Diagnostics;

namespace SmartConductor.Controllers
{
    public class HomeController : Controller
    {
        private static RailwayContext db = new RailwayContext();
        private static RailwayMap map;
        private static RailwayMap reversedMap;
        private static DistanceComputingCenter calculator;
        private static Object findSolutionLock = new object();

        public ActionResult Index()
        {
            return View();
        }



        public ActionResult Timetable()
        {
            //InitialiseMap();
            return View();
        }

        public static void InitialiseMap()
        {
            if (map == null || reversedMap == null || calculator == null)
            {
                map = new RailwayMap();
                var stations = db.RailwayStations.ToList<RailwayStation>();
                stations.ForEach(g => map.Stations.Add(g.Name, g));
                reversedMap = map.GetReversedMap();
                calculator = new DistanceComputingCenter();
            }
        }


        [HttpPost]
        [OutputCache(Duration = 360, VaryByParam = "*")]
        public ActionResult FindSolution(Request request, bool reversed)
        {
            if (!ModelState.IsValid)
            {
                if (ModelState["DateString"].Errors.Count > 0)
                {
                    ViewBag.DatetimeIsValid = false;
                }
                return View("Timetable", request);
            }
            else
            {
                ViewBag.DatetimeIsValid = true;
            }
            request.Start = Utilities.FirstCharToUpper(request.Start.ToLower());
            request.End = Utilities.FirstCharToUpper(request.End.ToLower());
            List<Solution> bestPaths;
            lock (findSolutionLock)
            {
                if (!reversed)
                {
                    bestPaths = calculator.ComputeSolutions(map, request.Start, request.End, request.Date);
                }
                else
                {
                    bestPaths = calculator.ComputeReversedSolutions(reversedMap, request.End, request.Start, request.Date);
                }

                string relativeMapFolderPath = "~/Maps/";
                string mapFolderPath = Server.MapPath(Url.Content(relativeMapFolderPath));
                for (int i = 0; i < bestPaths.Count; i++)
                {
                    Utilities.GenerateMap(bestPaths[i].Path, mapFolderPath, i.ToString() + ".png", ImageFormat.Png);
                }

                ViewBag.Extension = ".png";
                ViewBag.MapFolderPath = relativeMapFolderPath;
                ViewBag.Date = request.Date;

            }
            return View(bestPaths);
        }


        [OutputCache(Duration = 360, VaryByParam = "term")]
        public ActionResult TrainStationSuggestions(string term)
        {
            var stations = db.RailwayStations.Where(a => a.Name.ToLower().StartsWith(term.ToLower())).Take(4).Select(a => new { label = a.Name });
            return Json(stations, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = null;
            return View();
        }

        [HttpPost]
        public ActionResult Contact(Email email)
        {
            if (!ModelState.IsValid)
            {
                return View("Contact", email);
            }
            if (Utilities.SendAnEmail(email))
            {
                ViewBag.Message = "Email has been sent.";
            }
            else
            {
                ViewBag.Message = "Error. Try again later...";
            }

            return View("Contact");
        }


        public ActionResult AboutApplication()
        {
            return View();
        }

        //public ActionResult Error()
        //{
        //    var zero = 0;
        //    int a = 2 / zero;
        //    return View();
        //}
    }
}