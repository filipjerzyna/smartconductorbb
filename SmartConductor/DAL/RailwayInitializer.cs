﻿using SmartConductor.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SmartConductor.DAL
{
    public class RailwayInitializer : DropCreateDatabaseIfModelChanges<RailwayContext>
    {
        private List<Connection> connections = new List<Connection>();
        private List<TrainType> trainTypes = new List<TrainType>();
        private static Random random = new Random();
        protected override void Seed(RailwayContext context)
        {
            SeedRailwayData(context);
            base.Seed(context);
        }

        //Data for inversed graph testing
        private void SeedRailwayData(RailwayContext context)
        {
            //RailwayStation gdynia = new RailwayStation("Gdynia", 54.5188898, 18.530540900000005);
            //RailwayStation gdansk = new RailwayStation("Gdańsk", 54.35202520000001, 18.64663840000003);
            //RailwayStation szczecin = new RailwayStation("Szczecin", 52.3696663, 19.004718599999933);
            //RailwayStation olsztyn = new RailwayStation("Olsztyn", 53.778422, 20.48011930000007);
            //RailwayStation bialystok = new RailwayStation("Białystok", 53.13248859999999, 23.168840300000056);
            //RailwayStation poznan = new RailwayStation("Poznań", 52.406374, 16.925168100000064);
            //RailwayStation warszawa = new RailwayStation("Warszawa", 52.2296756, 21.012228700000037);
            //RailwayStation lodz = new RailwayStation("Łódź", 51.7592485, 19.45598330000007);
            //RailwayStation wroclaw = new RailwayStation("Wrocław", 51.1078852, 17.03853760000004);
            //RailwayStation kielce = new RailwayStation("Kielce", 50.8660773, 20.628567699999962);
            //RailwayStation lublin = new RailwayStation("Lublin", 51.2464536, 22.568446300000005);
            //RailwayStation katowice = new RailwayStation("Katowice", 50.26489189999999, 19.02378150000004);
            //RailwayStation krakow = new RailwayStation("Kraków", 50.06465009999999, 19.94497990000002);
            //RailwayStation rzeszow = new RailwayStation("Rzeszów", 50.0411867, 21.999119599999972);
            //RailwayStation zakopane = new RailwayStation("Zakopane", 49.299181, 19.94956209999998);

            RailwayStation glasgow = new RailwayStation("Glasgow", 55.864237, -4.251806);
            RailwayStation edinburgh = new RailwayStation("Edinburgh", 55.953252, -3.188267);
            RailwayStation newcastle = new RailwayStation("Newcastle", 54.978252, -1.617780);
            RailwayStation leeds = new RailwayStation("Leeds",53.800755 , -1.549077);
            RailwayStation manchester = new RailwayStation("Manchester", 53.480759, -2.242631);
            RailwayStation liverpool = new RailwayStation("Liverpool", 53.408371, -2.991573);
            RailwayStation birmingham = new RailwayStation("Birmingham", 52.486243, -1.890401);
            RailwayStation london = new RailwayStation("London", 51.507351, -0.127758);
            RailwayStation bristol = new RailwayStation("Bristol", 51.454513, -2.587910);
            RailwayStation cardiff = new RailwayStation("Cardiff", 51.481581, -3.179090);
           

            Company company = new Company()
            {
                CompanyId = 1,
                Name = "CrossCountry Trains"
            };

            TrainType trainType1 = new TrainType() { TrainTypeId = 1, CompanyId = 1, Company = company, ImagePath = "TrainIcon1.png", Name = "Local train" };
            TrainType trainType2 = new TrainType() { TrainTypeId = 2, CompanyId = 1, Company = company, ImagePath = "TrainIcon2.png", Name = "Fast train" };
            trainTypes.Add(trainType1);
            trainTypes.Add(trainType2);


            List<RailwayStation> stations = new List<RailwayStation>();
            stations.Add(glasgow);
            stations.Add(edinburgh);
            stations.Add(newcastle);
            stations.Add(leeds);
            stations.Add(manchester);
            stations.Add(liverpool);
            stations.Add(birmingham);
            stations.Add(london);
            stations.Add(bristol);
            stations.Add(cardiff);


            int amountOfConnections = 30;

            CreateConnections(glasgow, edinburgh, 30, amountOfConnections, true);
            CreateConnections(edinburgh, newcastle, 45, amountOfConnections, true);
            CreateConnections(glasgow, newcastle, 70, amountOfConnections, true);
            CreateConnections(newcastle, leeds, 45, amountOfConnections, true);
            CreateConnections(leeds, manchester, 25, amountOfConnections, true);
            CreateConnections(manchester, liverpool, 25, amountOfConnections, true);
            CreateConnections(manchester, birmingham, 45, amountOfConnections, true);
            CreateConnections(liverpool, birmingham, 50, amountOfConnections, true);
            CreateConnections(birmingham, london, 40, amountOfConnections, true);
            CreateConnections(birmingham, cardiff, 60, amountOfConnections, true);
            CreateConnections(birmingham, bristol, 40, amountOfConnections, true);
            CreateConnections(cardiff, bristol, 20, amountOfConnections, true);
            CreateConnections(bristol, london, 45, amountOfConnections, true);
            CreateConnections(manchester, london, 65, amountOfConnections, true);
            CreateConnections(liverpool, cardiff, 120, amountOfConnections, true);


            context.Companies.Add(company);
            context.TrainTypes.Add(trainType1);
            context.TrainTypes.Add(trainType2);
            stations.ForEach(g => context.RailwayStations.Add(g));
            connections.ForEach(g => context.Connections.Add(g));
            context.SaveChanges();
        }

        private void CreateConnections(RailwayStation station1, RailwayStation station2, int averageTravelTime, int quantity, bool returnConnection)
        {

            TrainType trainType;
            int hour;
            int minutes;
            int randomDifference;
            int randomDifferenceHours;
            int travelTime;
            for (int i = 0; i < quantity; i++)
            {
                randomDifference = random.Next(Convert.ToInt32(averageTravelTime * 0.2));
                travelTime = averageTravelTime + randomDifference;
                randomDifferenceHours = Convert.ToInt32(Math.Truncate(Convert.ToDouble(travelTime / 60)) + 1);
                hour = random.Next(24 - randomDifferenceHours);
                minutes = random.Next(60);
                trainType = trainTypes[random.Next(trainTypes.Count)];
                connections.Add(new Connection(station1, station2, travelTime, hour, minutes, trainType));
            }

            if (returnConnection)
            {
                for (int i = 0; i < quantity; i++)
                {
                    randomDifference = random.Next(Convert.ToInt32(averageTravelTime * 0.2));
                    travelTime = averageTravelTime + randomDifference;
                    randomDifferenceHours = Convert.ToInt32(Math.Truncate(Convert.ToDouble(travelTime / 60)) + 1);
                    hour = random.Next(24 - randomDifferenceHours);
                    minutes = random.Next(60);
                    trainType = trainTypes[random.Next(trainTypes.Count)];
                    connections.Add(new Connection(station2, station1, travelTime, hour, minutes, trainType));
                }
            }
        }

    }
}