﻿using SmartConductor.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace SmartConductor.DAL
{
    public class RailwayContext : DbContext
    {
        public DbSet<Connection> Connections { get; set; }
        public DbSet<RailwayStation> RailwayStations { get; set; }
        public DbSet<TrainType> TrainTypes { get; set; }
        public DbSet<Company> Companies { get; set; }
        public RailwayContext()
            : base("RailwayContext")
        {

        }

        static RailwayContext()
        {
            Database.SetInitializer<RailwayContext>(new RailwayInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>(); 
        }
    }
}