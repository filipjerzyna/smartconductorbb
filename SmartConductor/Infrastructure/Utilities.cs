﻿using SendGrid;
using SmartConductor.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace SmartConductor.Infrastructure
{
    public class Utilities
    {
        private static Dictionary<string, System.Drawing.Point> coordinates = new Dictionary<string, System.Drawing.Point>
        {
            //{"Szczecin", new System.Drawing.Point(70,202)},
            //{"Gdynia", new System.Drawing.Point(432,31)},
            //{"Gdańsk", new System.Drawing.Point(445,58)},
            //{"Olsztyn", new System.Drawing.Point(610,149)},
            //{"Białystok", new System.Drawing.Point(856,248)},
            //{"Poznań", new System.Drawing.Point(287,357)},
            //{"Warszawa", new System.Drawing.Point(660,381)},
            //{"Łódź", new System.Drawing.Point(519,452)},
            //{"Wrocław", new System.Drawing.Point(300,547)},
            //{"Kielce", new System.Drawing.Point(624,582)},
            //{"Lublin", new System.Drawing.Point(800,528)},
            //{"Katowice", new System.Drawing.Point(477,668)},
            //{"Kraków", new System.Drawing.Point(562,697)},
            //{"Rzeszów", new System.Drawing.Point(750,700)},
            //{"Zakopane", new System.Drawing.Point(563,806)}
            
            {"Glasgow", new System.Drawing.Point(279,309)},
            {"Edinburgh", new System.Drawing.Point(348, 302)},
            {"Newcastle", new System.Drawing.Point(424, 383)},
            {"Leeds", new System.Drawing.Point(426,506)},
            {"Manchester", new System.Drawing.Point(397, 534)},
            {"Liverpool", new System.Drawing.Point(359, 552)},
            {"Birmingham", new System.Drawing.Point(397, 637)},
            {"London", new System.Drawing.Point(517, 721)},
            {"Bristol", new System.Drawing.Point(370, 745)},
            {"Cardiff", new System.Drawing.Point(332, 728)}

        };


        public static double CalculateTimeSpanWithoutDate(DateTime date1, DateTime date2)
        {
            //Making a copy in order to compare only hour and minutes between two dates
            DateTime date1Temp = new DateTime(date2.Year, date2.Month, date2.Day, date1.Hour, date1.Minute, date1.Millisecond);
            double difference = (date2 - date1Temp).TotalMinutes;
            return Math.Abs(difference);
        }

        public static double CalculateTimeSpan(DateTime date1, DateTime date2)
        {
            double difference = (date2 - date1).TotalMinutes;
            return Math.Abs(difference);
        }

        public static void GenerateMap(List<Connection> connections, string path, string outputFilename, ImageFormat format)
        {
            string backgroundPath = path + "MapUK_Large.jpg";
            if (System.IO.File.Exists(backgroundPath))
            {

                Image mapBackground = Bitmap.FromFile(backgroundPath);
                System.Drawing.Pen pen = new System.Drawing.Pen(System.Drawing.Color.Firebrick, 6);
                using (var graphics = Graphics.FromImage(mapBackground))
                {

                    foreach (var connection in connections)
                    {
                        var startPoint = coordinates[connection.Start.Name];
                        var endPoint = coordinates[connection.End.Name];
                        graphics.DrawLine(pen, startPoint.X, startPoint.Y, endPoint.X, endPoint.Y);
                    }

                }

                mapBackground.Save(path + outputFilename, format);
            }
        }
        public static bool SendAnEmail(Email email)
        {

            // Create the email object first, then add the properties.
            var myMessage = new SendGridMessage();

            // Add the message properties.
            myMessage.From = new MailAddress("smartconductor@gmail.com");

            // Add multiple addresses to the To field.
            List<String> recipients = new List<String>
{
    @"Admin <smartconductor@gmail.com>"
};

            myMessage.AddTo(recipients);
            myMessage.Subject = "Message from the website";
            StringBuilder messageBody = new StringBuilder();
            messageBody.Append(email.Message);
            messageBody.Append("<br/>");
            messageBody.Append(email.Name);
            messageBody.Append(" ");
            messageBody.Append(email.Surname);
            messageBody.Append("<br/>");
            messageBody.Append(email.EmailAddress);
            myMessage.Html = messageBody.ToString();
            // Create network credentials to access your SendGrid account.
            var username = "azure_b775dc3a8233d1aa0ccf98b13534631c@azure.com";
            var pswd = "pYqo3Lp26TA20Yz";
            try
            {
                var credentials = new NetworkCredential(username, pswd);

                // Create an Web transport for sending email, using credentials...
                var transportWeb = new Web(credentials);

                // Send the email.
                transportWeb.DeliverAsync(myMessage);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static string FirstCharToUpper(string input)
        {
            if (String.IsNullOrEmpty(input))
                throw new ArgumentException("String is null or empty!");
            return input.First().ToString().ToUpper() + String.Join("", input.Skip(1));
        }
    }
}