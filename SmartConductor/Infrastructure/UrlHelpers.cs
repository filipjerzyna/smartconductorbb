﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartConductor.Infrastructure
{
    public static class UrlHelpers
    {
        public static string TrainTypesPath(this UrlHelper helper, string trainTypeFilename)
        {
            var trainTypesFolder = AppConfig.TrainTypesFolderRelative;
            var path = Path.Combine(trainTypesFolder, trainTypeFilename.Replace(".png", "Small.png"));
            var absolutePath = helper.Content(path);
            return absolutePath;
        }
    }
}