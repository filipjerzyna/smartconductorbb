﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace SmartConductor.Infrastructure
{
    public class AppConfig
    {
        private static string _trainTypesFolderRelative = ConfigurationManager.AppSettings["TrainTypesFolder"];

        public static string TrainTypesFolderRelative
        {
            get { return _trainTypesFolderRelative; }
        }

    }
}